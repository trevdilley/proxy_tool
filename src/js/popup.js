$( document ).ready(function() {
  
  $(document).on('change', 'input', function() {
    var value = $(this).val();
    $.ajax({
      type: "POST",
      url: 'http://devel.streamate.com/INTERNAL/ProxyConfig/',
      data: "change=1&backend="+value,
      success: function( response, textStatus, jqXHR ) {
        window.close();
      }
    });
  });
  
  setTimeout(function(){
    sendRequest('http://devel.streamate.com/INTERNAL/ProxyConfig/');
  }, 400);
  
});

var sendPost = function(url)
{
	$.ajax(
	{
	  url : url,
	  beforeSend : requestbeforeSend,
	  success : requestSuccess,
	  error : requestError
	});
}

var sendRequest = function(url)
{
	$.ajax(
	{
	  url : url,
	  beforeSend : requestbeforeSend,
	  success : requestSuccess,
	  error : requestError
	});
}

var requestbeforeSend = function(jqXHR, settings)
{
  $('div#error').hide();
  $('div#response').hide();
  $('div#loading').show();
}

var requestSuccess = function(response, textStatus, jqXHR)
{
   $('div#error').hide();
   $('div#loading').hide();
   
   layout = '<h1>Proxy Config</h1><ul class="row">';
   $(response).find('label').each(function()
   {
    var text = $(this).text();
    var id = $(this).attr('for');
    var inputfield = $( response ).find('input#'+id);
    var value = $( inputfield ).val();
    var checked = '';
    if($( inputfield ).is(":checked"))
    {
      var checked = 'checked="checked"';
    }
    layout = layout+'<li class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><label class="save"><input id="'+id+'" type="radio" name="backend" value="'+value+'" '+checked+' /> '+text+'</label></li>';
   });
  layout = layout+'</ul>'
   
   
   $('div#response').html(layout).show();
}

var requestError = function(jqXHR, textStatus, error)
{
  $('div#response').hide();
  $('div#loading').hide();
  $('div#error').html(error).show();
}