$(document).ready(function()
{
  sendRequest('http://devel.streamate.com/INTERNAL/ProxyConfig/');
});

var sendRequest = function(url)
{
  $.ajax(
  {
    url : url,
    success : requestSuccess,
    error : requestError
  });
}

var requestSuccess = function(response, textStatus, jqXHR)
{
  $(response).find('label').each(function()
  {
    var text = $(this).text();
    var id = $(this).attr('for');
    var inputfield = $(response).find('input#' + id);
    var value = $(inputfield).val();
    var checked = false;
    if ($(inputfield).is(":checked")) {
      var checked = true;
    }
    chrome.contextMenus.create(
    {
      "type" : "radio",
      "id" : value,
      "title" : text,
      "checked" : checked,
      "onclick" : genericOnClick
    });
  });
}

var requestError = function(jqXHR, textStatus, error)
{
  console.log('Error');
}

function genericOnClick(info, tab)
{
  var value = info.menuItemId;
  $.ajax(
  {
    type : "POST",
    url : 'http://devel.streamate.com/INTERNAL/ProxyConfig/',
    data : "change=1&backend=" + value
  });
}
